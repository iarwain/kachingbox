# README #

![kaching.jpg](https://bitbucket.org/repo/ep6x5M/images/1196941990-kaching.jpg)

### What is Kaching! Box for? ###

This is intended to be a benchmarking program for the Orx Portable Game Engine to measure the FPS when there are many objects, some with bodies and some without, in order to test on various resolutions and machines.

It is also intended that the assets and code can be ported to other engines so that Orx can be compared to its competition using typical game scenarios, ie:

* Many objects
* Physics
* Effects
* Sound

### How do I get set up? ###

.exe's are included but you'll need to supply your own orx.dll or orxd.dll.

* There is a premake file if you want to build from scratch
* You will need a rebuilt Orx with libs and includes if so.

### Controls  ###

* A Key to increase Drones
* Z Key to decrease Drones
* S Key to increase Insects
* X Key to decrease Insects
* Esc to quit

### Contribution guidelines ###

* Please test or fix if you wish.
* If you use another 2D engine apart from Orx, feel free to port and compare!

### Who do I talk to? ###

* Wayne: sausage@zeta.org.au
* Or visit: https://gitter.im/orx/orx