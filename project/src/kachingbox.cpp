/* Kaching Box
 *
 * Copyright (c) 2016 waynejohnson.net
 *
 * This software is licensed under the
 * Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * While the license does not allow commercial use, I permit the
 * use (but not sale) of the software commercially.
 * This license will change over time to fit more appropriately.
 *
 */

/**
 * @date started October 2016, finished ---
 * @author sausage@zeta.org.au
 *
 */

#include "orx.h"

orxBOOL		isQuitting;
orxS32		droneCount;
orxS32		insectCount;
orxS32		maxDrones;
orxS32		maxInsects;

orxBOOL IsInputPressed(const orxSTRING input){
	return orxInput_IsActive(input) && orxInput_HasNewStatus(input);
}

void PlaySoundOn(orxOBJECT *object, orxSTRING soundFromConfig)
{
	if (orxObject_GetLastAddedSound(object) != orxNULL){
		orxObject_RemoveSound(object, soundFromConfig);
	}
	orxObject_AddSound(object, soundFromConfig);
}

void KillEnemiesOutsideTheBoundary(){
	for (orxOBJECT *object = orxOBJECT(orxStructure_GetFirst(orxSTRUCTURE_ID_OBJECT));
		 object != orxNULL;
		 object = orxOBJECT(orxStructure_GetNext(object))){

		orxConfig_PushSection(orxObject_GetName(object));
		if (orxConfig_GetBool("IsInsect") || orxConfig_GetBool("IsDrone")){
			orxVECTOR objectPosition;
			orxObject_GetPosition(object, &objectPosition);

			if (objectPosition.fX < 0 || objectPosition.fX > 1920 || objectPosition.fY < -500 || objectPosition.fY > 1080){
				orxObject_SetLifeTime(object, orxFLOAT_0);
			}
		}
		orxConfig_PopSection();
	}
}

void CreateKillerDrone(){
  if (droneCount < maxDrones)  {
	orxObject_CreateFromConfig("DroneObject");
  }
}

void CreateInsect(){
	if (insectCount < maxInsects){
		orxObject_CreateFromConfig("InsectObject");
	}
}

orxSTATUS orxFASTCALL ObjectEventHandler(const orxEVENT *_pstEvent){
	if (isQuitting == orxTRUE){
		return orxSTATUS_SUCCESS;
	}

	if (_pstEvent->eID ==  orxOBJECT_EVENT_CREATE){
		orxConfig_PushSection(orxObject_GetName(orxOBJECT(_pstEvent->hSender)));
		if (orxConfig_GetBool("IsDrone")){
			droneCount++;
		}
		else if (orxConfig_GetBool("IsInsect")){
			insectCount++;
		}
		orxConfig_PopSection();
	}
	else if (_pstEvent->eID ==  orxOBJECT_EVENT_DELETE){
		orxConfig_PushSection(orxObject_GetName(orxOBJECT(_pstEvent->hSender)));
		if (orxConfig_GetBool("IsDrone")){
			droneCount--;
			CreateKillerDrone();
		}
		else if (orxConfig_GetBool("IsInsect")){
			insectCount--;
		}
		orxConfig_PopSection();
	}

  return orxSTATUS_SUCCESS;
}

void WriteStringToReport(const orxSTRING string, orxBOOL append){
	orxS32 flags = orxFILE_KU32_FLAG_OPEN_APPEND | orxFILE_KU32_FLAG_OPEN_WRITE;
	if (append == orxFALSE){
		flags =  orxFILE_KU32_FLAG_OPEN_WRITE;
	}
	
	orxFILE *file = orxFile_Open ("fps-report.txt",	flags); 	

	orxFile_Print(file, string);

	orxFile_Close(file);
}

void orxFASTCALL TimerUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext){
	if (droneCount < maxDrones){
		CreateKillerDrone();
	}

	if (insectCount < maxInsects){
		CreateInsect();
	}

	KillEnemiesOutsideTheBoundary();

	orxCHAR row[30];
	orxString_NPrint(row, 29, "%d\t%d\t%d\n", orxStructure_GetCounter(orxSTRUCTURE_ID_OBJECT), orxStructure_GetCounter(orxSTRUCTURE_ID_BODY),  
												orxFPS_GetFPS() );

	WriteStringToReport(row, orxTRUE);

}

void orxFASTCALL InputUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext){
	if (IsInputPressed("MoreDrones")){
		maxDrones = orxMIN(100, maxDrones + 1);
	}
	else if (IsInputPressed("LessDrones")){
		maxDrones = orxMAX(0, maxDrones - 1);
	}

	if (IsInputPressed("MoreInsects")){
		maxInsects = orxMIN(100, maxInsects + 1);
	}
	else if (IsInputPressed("LessInsects")){
		maxInsects = orxMAX(0, maxInsects - 1);
	}
}

void EffectAt(const orxVECTOR &position, const orxSTRING effect){
	orxOBJECT *object = orxObject_CreateFromConfig(effect);
	if (object){
		orxObject_SetPosition(object, &position);
	}
}

void BulletFlashAt(const orxVECTOR &position){
	EffectAt(position, "BulletFlash");
}

void ExplosionAt(const orxVECTOR &position){
	EffectAt(position, "ExplosionsObject");
}

orxSTATUS orxFASTCALL PhysicsEventHandler(const orxEVENT *_pstEvent)
{
	if (_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD){
		orxPHYSICS_EVENT_PAYLOAD *pstPayload;
		pstPayload = (orxPHYSICS_EVENT_PAYLOAD *)_pstEvent->pstPayload;

		/* Gets colliding objects */
		orxOBJECT *recipient, *sender;
		recipient	= orxOBJECT(_pstEvent->hRecipient);
		sender		= orxOBJECT(_pstEvent->hSender);

		if (recipient != orxNULL && sender != orxNULL){
			struct
			{
			  orxOBJECT *self, *other;
			} configurations[] = {
			  {recipient, sender},
			  {sender, recipient}
			};

			orxBOOL stop = orxFALSE;
			for (orxU32 i = 0; i < orxARRAY_GET_ITEM_COUNT(configurations) && !stop; i++){
				orxOBJECT *self = configurations[i].self;
				orxOBJECT *other = configurations[i].other;

				orxConfig_PushSection(orxObject_GetName(self));

				// Bumper?
				if (orxConfig_GetBool("IsBumper")){
					orxVECTOR upVector = {0.0f, -300.0f, 0.0f};
					orxObject_SetTargetAnim(self, "SpringUp");
					orxObject_ApplyImpulse(other, &upVector, orxNULL);
					PlaySoundOn(self, "Bounce");
					stop = orxTRUE;
				}
				// Bullet?
				else if (orxConfig_GetBool("IsBullet")){
					orxConfig_PushSection(orxObject_GetName(other));

					// With insect?
					if (orxConfig_GetBool("IsInsect")){
						orxVECTOR insectLocation;
						orxObject_GetPosition(other, &insectLocation);
						ExplosionAt(insectLocation);
						orxObject_SetLifeTime(other, orxFLOAT_0);
						CreateInsect();
					}
					// With drone?
					else if (orxConfig_GetBool("IsDrone")){
						orxVECTOR droneLocation;
						orxObject_GetPosition(other, &droneLocation);
						ExplosionAt(droneLocation);
						orxObject_SetLifeTime(other, orxFLOAT_0);
					}
					// With playfield
					else{
						orxVECTOR bulletLocation;
						orxObject_GetPosition(self, &bulletLocation);
						BulletFlashAt(bulletLocation);
					}

					orxObject_SetLifeTime(self, orxFLOAT_0);

					orxConfig_PopSection();
					stop = orxTRUE;
				}

				orxConfig_PopSection();
			}
		}
	}

	return orxSTATUS_SUCCESS;
}

/** Run callback for standalone
 */
orxSTATUS orxFASTCALL Run()
{
	orxSTATUS eResult = orxSTATUS_SUCCESS;

	if (orxInput_IsActive("Quit")){
	eResult = orxSTATUS_FAILURE;
	}

	orxConfig_SetS32("ObjectCount", orxStructure_GetCounter(orxSTRUCTURE_ID_OBJECT));
	orxConfig_SetU32("FPSCount", orxFPS_GetFPS());

	orxConfig_SetS32("MaxDrones", maxDrones);
	orxConfig_SetS32("MaxInsects", maxInsects);

	return eResult;
}

void orxFASTCALL Exit()
{
	isQuitting = orxTRUE;
	orxLOG("=================== EXIT ================");
}


orxSTATUS orxFASTCALL Init()
{
	orxViewport_CreateFromConfig("Viewport");
	orxObject_CreateFromConfig("Scene");

	orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_OBJECT, ObjectEventHandler);

	orxClock_AddGlobalTimer(TimerUpdate, orxFLOAT_1, -1, orxNULL);
	orxClock_Register(orxClock_FindFirst(orx2F(-1.0f), orxCLOCK_TYPE_CORE), InputUpdate, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_HIGH);

	orxConfig_PushSection("Game");

	maxDrones	= orxConfig_GetS32("MaxDrones");
	maxInsects	= orxConfig_GetS32("MaxInsects");
	droneCount	=
	insectCount	= 0;
	isQuitting	= orxFALSE;

	orxConfig_PopSection();

	// All future config queries made here will be in the 'Runtime' section by default
	orxConfig_PushSection("Runtime");

	WriteStringToReport("Objects\tBodies\tFPS\n", orxFALSE);

	return orxSTATUS_SUCCESS;
}

int main(int argc, char **argv)
{
	orx_Execute(argc, argv, Init, Run, Exit);

	return EXIT_SUCCESS;
}

#ifdef __orxMSVC__
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
  orx_WinExecute(Init, Run, Exit);

  return EXIT_SUCCESS;
}
#endif // __orxMSVC__
